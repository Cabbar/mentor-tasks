import React from 'react';
import './resetCSS/reset.css';
import './App.css';
import Navbar from "./components/navbar";
import MainPage from "./components/main-page";
function App() {
  return (
    <div className="App">
      <Navbar/>
      <MainPage/>

    </div>
  );
}

export default App;
