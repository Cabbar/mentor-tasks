import React from "react";

import './css/singl-mail.css'
class SingleMail extends React.Component{
    render() {
        return (
            <div className={'single-mail'} onClick={(event => {
                let a = event.currentTarget.children[1];
              if (  a.style.display === "none"){
                  a.style.display = "block";
              }
              else   a.style.display = "none";
            })}>
<div className="mail-initial">
    <p className="mail-owner">Blah</p>
    <p className="mail-title">Blah Blah</p>
</div>
                <div className="mail-context">
                    <p className="mail-context-txt">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, animi aut commodi distinctio dolorem eveniet iste itaque laboriosam, minima neque nostrum numquam odit quia sint voluptates. Dolorum eum officia reprehenderit!
                    </p>
                </div>
            </div>
        );
    }
}

export default SingleMail;

