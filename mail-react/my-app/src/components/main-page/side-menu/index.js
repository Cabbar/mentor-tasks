import React from "react";
import "./css/side-menu.css"

class SideMenu extends React.Component {
    render() {
        return (
            <div className={"side-menu"}>
                <div className="inbox folder">
                    <i className="fas fa-inbox folder-logo"> </i>
                    <p className="folder-name">Inbox</p>

                </div>
                <div className="sent folder">
                    <i className="fas fa-paper-plane folder-logo"> </i>
                    <p className="folder-name">Sent</p>
                </div>
                <div className="draft folder">
                    <i className="fas fa-sticky-note folder-logo"></i>
                    <p className="folder-name">Draft</p>
                </div>
            </div>
        )
    }
}

export default SideMenu;