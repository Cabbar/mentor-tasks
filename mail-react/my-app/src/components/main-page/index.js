import React from "react";
import "./css/main-page.css";
import SideMenu from "./side-menu";
import MainContainer from "./main-content-container";
class MainPage extends React.Component{
    render() {
        return(
            <div className={"main-page"}>
<SideMenu/>
<MainContainer/>
            </div>
        )
    }
}
export default MainPage;