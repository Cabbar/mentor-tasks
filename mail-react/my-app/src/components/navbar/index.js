import React from "react";
import "./css/navbar.css";
class Navbar extends React.Component{
render() {
    return(
        <div className={"nav"}>
            <img className={'logo-img'} src={require("../img/letter-icon.png")} alt=""/>
            <p className="logo-txt">J-Mail</p>

        </div>
    )
}
}

export default Navbar;