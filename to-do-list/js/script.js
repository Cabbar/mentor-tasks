//create single task item
let id = 0;

let createSingleTask = function(tskHead,tskDesc, tskDeadln){

    let taskHeader = document.createElement('p');
    taskHeader.className = "task-header";
    let taskDesc = document.createElement('p');
    taskDesc.className = "task-description";
    let taskDeadline = document.createElement('p');
    taskDeadline.className = "task-deadline";
    let taskContent = document.createElement('div');
    taskContent.className = "task-content";

    let deleteTaskBtn = document.createElement('i');
    deleteTaskBtn.className+="fas fa-trash-alt delete-task-btn";
    let taskItem = document.createElement('div');
    taskItem.className = "task-item";
    taskItem.ondragstart = function(event){
        event.dataTransfer.setData('text/plain', event.target.id);

    };

    taskItem.id = id++;
    taskItem.draggable = true;
    taskContent.append(taskHeader,taskDesc,taskDeadline);
    taskItem.append(taskContent,deleteTaskBtn);
    taskHeader.innerText = tskHead;
    taskDesc.innerText = tskDesc;
    taskDeadline.innerText = tskDeadln;
    return taskItem;
}


//local storage
let backlogTskId = 1
let todoTskId = 1
let inprgTskId = 1
let doneTskId = 1;
(function () {
    let storageItems = [];
if (localStorage.length>0){
    for (let count = 0;count<localStorage.length; count++){
    storageItems.push(JSON.parse(localStorage.getItem(localStorage.key(count))));
    if (storageItems[count].owner === 'backlog'){
        let tasks = document.querySelectorAll(".tasks");
tasks[0].append(createSingleTask(storageItems[count].header, storageItems[count].desc,storageItems[count].deadline));

    }
    else if (storageItems[count].owner === 'todo'){
        let tasks = document.querySelectorAll(".tasks");
        tasks[1].append(createSingleTask(storageItems[count].header, storageItems[count].desc,storageItems[count].deadline));

    }
    else if (storageItems[count].owner === 'inprogress'){
        let tasks = document.querySelectorAll(".tasks");
        tasks[2].append(createSingleTask(storageItems[count].header, storageItems[count].desc,storageItems[count].deadline));

    }
    else if (storageItems[count].owner === 'done'){
        let tasks = document.querySelectorAll(".tasks");
        tasks[3].append(createSingleTask(storageItems[count].header, storageItems[count].desc,storageItems[count].deadline));

    }
    }
}
    })();


//open window and render data from it
let  tasks = document.querySelectorAll(".tasks");

let addCardBtn = document.querySelectorAll('.add-card-btn');
let addWindow= document.querySelector('.add-task-panel');
let windowAddBtn = document.querySelector('.window-add-task-btn');
let indexOfCard = 0;
for (let i = 0;i<addCardBtn.length;i++){

addCardBtn[i].addEventListener("click",function (event) {
    addWindow.style.display = "flex";
indexOfCard = i;
})

};
windowAddBtn.addEventListener('click',function (event) {

    let taskHeaderText = document.getElementById('taskHeader').value;
    let taskDescText = document.getElementById('taskDesc').value;
    let taskDeadlineText = document.getElementById('taskDeadline').value;

    if (taskHeaderText !=="" && taskDescText !=="" && taskDeadlineText!==''){
let newTask = createSingleTask(taskHeaderText,taskDescText,taskDeadlineText)
tasks[indexOfCard].appendChild(newTask);
        addWindow.style.display = "none";
        let task = {
            header: taskHeaderText,
            desc: taskDescText,
            deadline: taskDeadlineText
        };
        if (tasks[indexOfCard].parentElement.id==='backLog'){

            let taskNumber = `task${backlogTskId=newTask.id}`
            task.owner = "backlog";
            localStorage.setItem(taskNumber, JSON.stringify(task));
        }
        else if (tasks[indexOfCard].parentElement.id==='toDo'){

            let taskNumber = `task${todoTskId=newTask.id}`
            task.owner = "todo";
            localStorage.setItem(taskNumber, JSON.stringify(task));

        }
        else if (tasks[indexOfCard].parentElement.id==='inProgress'){

            let taskNumber = `task${inprgTskId=newTask.id}`
            task.owner = "inprogress";
            localStorage.setItem(taskNumber, JSON.stringify(task));

        }
        else if (tasks[indexOfCard].parentElement.id==='done'){

            let taskNumber = `task${doneTskId=newTask.id}`
            task.owner = "done";
            localStorage.setItem(taskNumber, JSON.stringify(task));

        }

    }
    else alert("Please fill the Inputs");


})



//close window
let closeWindowBtn = document.querySelector('.close-window-btn');
closeWindowBtn.addEventListener('click' , function (event) {
addWindow.style.display = "none";
});



// drag drop
for(let elems of tasks){
    elems.ondragover = function (event) {
        event.preventDefault();

    }
    elems.ondrop = function (event) {
        const id = event.dataTransfer.getData('text');
        const draggedElement = document.getElementById(id);
        const dropzone = event.target;
        if (dropzone.className==="tasks"){
            dropzone.appendChild(draggedElement);
            event.dataTransfer.clearData();


        }

    }
}

//delete task

let deleteBtn = document.querySelectorAll('.delete-task-btn');
for (let elems of deleteBtn){
    elems.addEventListener('click', function (event) {
        console.log(event.target.parentElement.parentElement.parentElement.id)
        if (event.target.parentElement.parentElement.parentElement.id === 'backLog'){
            localStorage.removeItem(`task${event.target.parentElement.id}`)
        }
        else if (event.target.parentElement.parentElement.parentElement.id === 'toDo'){
            localStorage.removeItem(`task${event.target.parentElement.id}`)
        }
        else if (event.target.parentElement.parentElement.parentElement.id === 'inProgress'){
            localStorage.removeItem(`task${ event.target.parentElement.id}`)
        }
        else if (event.target.parentElement.parentElement.parentElement.id === 'done'){
            localStorage.removeItem(`task${event.target.parentElement.id}`)
        }
        event.target.parentElement.parentElement.removeChild(event.target.parentElement);


    })
}
